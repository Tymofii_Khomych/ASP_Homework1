﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;

namespace test.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
